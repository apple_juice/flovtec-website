"use strict";

window.addEventListener('DOMContentLoaded', function () {
  // START OF: is mobile =====
  function isMobile() {
    return (/Android|iPhone|iPad|iPod|BlackBerry/i).test(navigator.userAgent || navigator.vendor || window.opera);
  }
  // ===== END OF: is mobile

  const BODY = $('body');

  // START OF: mark is mobile =====
  (function() {
    BODY.addClass('loaded');

    if(isMobile()){
      BODY.addClass('body--mobile');
    }else{
      BODY.addClass('body--desktop');
    }
  })();
  // ===== END OF: mark is mobile

  function initInputBlur() {
    $('.form_subscribe').each(function () {
      $(this).find('.form-control').on('blur', function(event) {
        let inputValue = this.value;
        if (inputValue) {
          this.classList.add('value-exists');
        } else {
          this.classList.remove('value-exists');
        }
      });
    })
  }

  initInputBlur();

  if ($('._text_truncate').length) {
    $('._text_truncate').each(function () {
      $(this).truncate({
        lines: 5,
      });

      $('.read_more_link').data('truncate', true)

      $(this).siblings('.read_more_link').on('click', function (e) {
        e.preventDefault();

        let isTruncated = $(this).data('truncate');

        console.log(isTruncated)

        if (isTruncated === false) {
          $(this).data('truncate', true);
          $(this).text('Read more')
          $(this).siblings('._text_truncate').truncate('collapse')

        } else {
          $(this).data('truncate', false);
          $(this).text('Show less')
          $(this).siblings('._text_truncate').truncate('expand')
        }
      })
    })
  }
});

$(document).ready(function () {
  let controller = new ScrollMagic.Controller();

  let images = $('.feature_images__image').length;

  for (let i = 0; i < images; i++) {
    let parallaxHeight= $(".feature_block--"+i).outerHeight(),
      duration = parallaxHeight;
    if (i === 5 ) {
      duration = 2
    }
    new ScrollMagic.Scene({
      triggerElement: ".feature_block--"+i,
      duration: duration,
      offset: parallaxHeight/2  - 100
    })
      .setPin(".feature_images")
      .setClassToggle(".feature_images", "feature_images--"+i)
      .addTo(controller);

  }

  new ScrollMagic.Scene({
    triggerElement: ".feature_block--5",
    offset: $(".feature_block--5").outerHeight()/2 - 100
  })
    .setPin(".feature_images")
    .setClassToggle(".feature_images", "last-item")
    .addTo(controller);

  $( ".teaser.blue" ).fadeOut( );
  $( ".teaser.purple" ).fadeIn( "slow");

  var $element = $('.teaser.blue');

  setInterval(function(){
    console.log($element.is(':visible'));
    if ( $element.is(':visible') ) {
      $( ".teaser.blue" ).fadeOut("slow");
      $( ".teaser.purple" ).fadeIn("slow");
    } else {
      $( ".teaser.blue" ).fadeIn("slow");
      $( ".teaser.purple" ).fadeOut("slow");
    }
  }, 7500);

  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
      $('.btn_scroll_top').fadeIn();
    } else {
      $('.btn_scroll_top').fadeOut();
    }
  });

  //Click event to scroll to top
  $('.btn_scroll_top').on('click', function(){
    $('html, body').animate({scrollTop : 0},0);
    return false;
  });
})